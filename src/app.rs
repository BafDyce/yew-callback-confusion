use yew::{html, callback::Callback, Component, ComponentLink, Html, Renderable, ShouldRender, services::console::ConsoleService};

use crate::{
    Map,
};
use super::map::MapMsg;

#[derive(Debug, Default)]
pub struct App {
    map: Map,
}

pub enum AppMsg {
    HandleMapMsg(MapMsg),
    Nop,
}

impl Component for App {
    type Message = AppMsg;
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        App::default()
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            AppMsg::HandleMapMsg(map_msg) => {
                self.map.update(map_msg);
                false
            }
            AppMsg::Nop => {
                ConsoleService::new().log("Nop triggered");
                false
            }
        }
    }
}

impl Renderable<App> for App {
    fn view(&self) -> Html<Self> {
        ConsoleService::new().log(&format!("App.view(): {:?}", self));

        html! {
            <div>
                <h1>{"Yew - compilation panic"}</h1>

                <br />
                <Map
                    map = &self.map
                    onsignal = Some(Callback::from(|msg: MapMsg| -> AppMsg {AppMsg::Nop}))
                    //onsignal = None
                />
/*
    The code above leads to the following compilation error:

error[E0271]: type mismatch resolving `<[closure@src/app.rs:52:52: 52:89] as std::ops::FnOnce<(map::MapMsg,)>>::Output == ()`
  --> src/app.rs:52:37
   |
52 |                     onsignal = Some(Callback::from(|msg: MapMsg| -> AppMsg {AppMsg::Nop}))
   |                                     ^^^^^^^^^^^^^^ expected enum `app::AppMsg`, found ()
   |
   = note: expected type `app::AppMsg`
              found type `()`
   = note: required because of the requirements on the impl of `std::convert::From<[closure@src/app.rs:52:52: 52:89]>` for `yew::Callback<map::MapMsg>`
   = note: required by `std::convert::From::from`

error: aborting due to previous error
For more information about this error, try `rustc --explain E0271`.
error: Could not compile `yew-callback-confusion`.
*/
            </div>
        }
    }
}
