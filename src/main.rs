#![recursion_limit="256"]
extern crate yew;

mod app;
mod map;

use app::App;
use map::{
    Map,
};


fn main() {
    yew::start_app::<App>();
}