use super::app::AppMsg;

use std::collections::HashMap;
use yew::{
    callback::Callback,
    Component,
    ComponentLink,
    html,
    Html,
    Renderable,
    services::console::ConsoleService,
    ShouldRender,
};


#[derive(Clone, Debug, PartialEq)]
pub struct Map {
    text: String,
    onsignal: Option<Callback<MapMsg>>,
}

pub enum MapMsg {
    ReverseText,
}

#[derive(Clone, Default, PartialEq)]
pub struct MapProperties {
    pub map: Map,
    pub onsignal: Option<Callback<MapMsg>>,
}

impl Map {
    fn draw_text(&self) -> Html<Map> {
        html!{
            <font onclick=|_| MapMsg::ReverseText>
                { &self.text }
            </font>
        }
    }
}

impl Component for Map {
    type Message = MapMsg;
    type Properties = MapProperties;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        let mut map = props.map;
        map.onsignal = props.onsignal;

        map
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        let retval = match msg {
            MapMsg::ReverseText => {
                self.text = self.text.chars().rev().collect::<String>();
                true
            }
        };

        if let Some(onsignal) = &self.onsignal {
            onsignal.emit(msg);
        }

        retval
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        *self = props.map;
        self.onsignal = props.onsignal;

        true
    }
}

impl Renderable<Map> for Map {
    fn view(&self) -> Html<Self> {
        ConsoleService::new().log(&format!("Map.view: {:?}", self));


        html! {
            <div class="map">
                { self.draw_text() }
            </div>
        }
    }
}

impl Default for Map {
    fn default() -> Map {
        Map {
            text: "This is a sample text".to_string(),
            onsignal: None,
        }
    }
}
